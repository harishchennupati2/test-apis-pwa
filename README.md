# AssetInvoiceTracking

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.

## Installing dependencies
- upgrade your angular cli to the latest version
```
$ npm uninstall -g angular-cli
$ npm cache clean or npm cache verify #(if npm > 5)
$ npm install -g @angular/cli@latest
```
- install app dependencies
```
$ npm install
```

## Development deploy
```
$ ng serve
```
- Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Testing on IE 11
- To test / deploy on IE 11
```
$ ng serve --configuration es5
```

## Build
```
$ ng build
```
- or for the production build:
```
$ ng build --prod
```

## Lint
```
$ ng lint
```

## Logging In and User Roles
- The app is currently wired to Mock API hosted at `https://pacific-journey-95032.herokuapp.com/api/v1/` (you can change this in the `environment.ts` files)
- So the follow is subject to changes made in the mock database hosted there
- There are 5 different types of User Roles demonstrated:
  - EndCustomer ( ashish :: password )
  - LeasingPartner ( jim :: password )
  - Supply Chain Team ( sesse :: password )
  - InstallationPartner ( gaurav :: password )
  - ManufacturingPartner ( mahesh :: password )
- Auth guards are enabled on certain pages, so not all users can access all pages

## Theming Config
- This app uses a completely customizable theming structure
- There are 3 themes defined: `default`, `reddish` and `dark` currently
- You can find these theme definitions in the `src/app/assets/scss/themes.scss` file
- Each color variable name has comments to help you figure out where all in the app that color is applied
- Every `Customer` can be mapped to a theme. You can find this mapping in the `src/app/configs/themes.config.ts`
- It's a simple map of *customerorgname* to *themename*
- If no theme is explicitly defined for a customer, the default theme is applied

## Theme Styling for Developers
- Any where in the scss files you can find examples of how to style components in a themified way
- You just have to user the `themify` mixin and then reference the color variables like `themed(<<COLOR_VARIABLE_NAME>>)`
- **IMPORTANT NOTE** : Any new *angular component* you build should use the `appThemed` **directive** from SharedModule
- Not doing so, will result in the *default theme* **always** being applied for that component (like intentionally done with `LoginComponent`)
- This is because Angular uses ViewEncapsulation, and css defiend inside components, cannot reference css classes outside the component; and the way theming works is that if an element has class `theme-dark` applied to it, then all its children and itself, will reference the *Dark Theme* colors


## Miscellaneous Configs
- All config files can be found in the `src/app/configs` folder

## Data Object Models
- Can be found in `src/app/core/models` folder

## PWA
- The APP is a PWA and works in offline mode
- It can cache the API's even when there is no internet connection
- It provides an app like experience
- You can use the app without wasting your internet resources


## PWA Installation
- You can install this PWA on a Desktop as a Desktop app
- You can install this PWA on your mobile as native mobile app
