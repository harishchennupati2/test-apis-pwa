# Asset and Invoice Tracking App

- Demo video https://youtu.be/JvlU1jvdp-0

## Themes
- I've configured the app to use 3 themes currently. 
- To show the flexibility and power of the themification, I've changed a lot of colors
- THEY DO NOT LOOK GOOD because I didn't try hard to make them look good.
- For the purpose of review of various roles in the app, please got to `src/app/configs/themes.config.ts` and make the object and empty object!!
- Then once you're done with review, you can apply the themes again, and play around with various color changes etc in `src/assets/scss/themes.scss`


## Requirement Items Not Done
  - Order Item status update (for installation partner) 
  - Order Item status bar arrow shaped boxes
  - PWA
  - PUT request for Invoice Creattion / Editing. (NOTE: The UI and all frontend functionatlity is complete)

## General comments
- The last two incomplete requirements mentioned are probably easy, but I'm too exhausted to write more code.
- All other requirements (and there were a lot), are complete I think.
- Themefying was the hardest part of the app (I don't think Angular Material Theming would have done the job, it doesn't allow these many colors as variables). Pluse not sure if even apart from that it would work as intended (need to change colors of svg charts too).. but I could be wrong about that
- Mock API integration was a close second tough thing. API is a bit of a mess. Same objects have different key properties in different api responses etc. Tough to generalize and make reusable components. But I did the most I could in code quality. eg. UtilService handles all Status to Color (themed) mappings because statuses are returned in different case in different API. sometimes with space, without space etc etc.
