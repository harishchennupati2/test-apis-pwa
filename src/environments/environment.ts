// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  apiBase: {
    EndCustomer: [
      'https://pacific-journey-95032.herokuapp.com/api/v1/',
      'https://pacific-journey-95032.herokuapp.com/api/v1/'
    ],
    LeasingPartner: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    SupplyChainTeam: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    ManufacturingUnit: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    InstallationPartner: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    default: 'https://pacific-journey-95032.herokuapp.com/api/v1/'
  },
  auth: {
    URLS: {
      base: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
      backend:'http://localhost:4200/',
      login: 'oauth/authorize',
      logout: 'oauth/logout',
      getme:'api/v1/me?'
    },
    clientId: 'dscfzami9szhhnxB8INsLsivwPjIzVHJ',
    clientSecret: 'secret',
    redirectURI: 'http://localhost:4200/login',
    logoutRedirectURI: 'http://localhost:4200/login'
  },
  users: [
    {
      userName: 'ashish@gmail.com',
      role: 'EndCustomer',
      nodeIndex: 0,
      customerorgname: 'customer1',
      signatoryposition: 'CEO'
    },
    {
      userName: 'oven@gmail.com',
      role: 'EndCustomer',
      nodeIndex: 1,
      customerorgname: 'customer1',
      signatoryposition: 'IT'
    },
    {
      userName: 'jim@gmail.com',
      role: 'LeasingPartner',
      customerorgname: 'customer2',
      signatoryposition: 'chairman'
    },
    {
      userName: 'sesse@gmail.com',
      role: 'SupplyChainTeam',
      customerorgname: 'customer3',
      signatoryposition: 'IT'
    },
    {
      userName: 'gaurav@gmail.com',
      role: 'InstallationPartner',
      signatoryposition: 'programmer'
    },
    {
      userName: 'mahesh@gmail.com',
      role: 'ManufacturingUnit',
      signatoryposition: 'manager'
    }
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
