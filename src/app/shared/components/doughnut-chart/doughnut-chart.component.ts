import { Component, OnInit, Input, OnChanges } from '@angular/core';

class ArcData {
  value: number;
  color: string;
  label?: string;
  strokeOffset?: number;
  angleOffset?: number;
  line?: { x: number, y: number };
  labelPosition?: { top: string, left: string, right: string };
}

/**
 * doughtnut chart compoennt
 * the svg based doughnut chart
 * no configuration required for generalization
 * just accepts a list of value (number) and  color pairs, and does everything else
 */
@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.scss']
})
export class DoughnutChartComponent implements OnInit, OnChanges {

  @Input() data: { value: number, color: string, label?: string }[];
  public graphData: ArcData[];

  public height = 100;
  public strokeDasharray = 2 * Math.PI * (this.height / 2 - 5);
  public total: number;

  constructor() { }

  ngOnInit() {
    this.setup();
  }

  /**
   * component on chnages
   * if height chnaged, setup chart based on new height
   */
  ngOnChanges() {
    this.setup();
  }


  /**
   * setup
   * calculate the length, positions, angles, of arcs and labels
   */
  private setup() {
    // copy the data into a new array, such that two consecutive small values aren't close by
    // this is to avoid overlapping labels as much as possible
    // only take the non-zero values
    const sortedData = this.data.filter(d => d.value);
    sortedData.sort((a: ArcData, b: ArcData) => {
      if (!a.label && b.label) { return -1; }
      if (a.label && !b.label) { return 1; }
      if (a.value < b.value) { return -1; }
      if (a.value > b.value) { return 1; }
      return 0;
    });
    this.graphData = [];
    for (let i = 0; i < this.data.filter(d => d.value).length; i++) {
      const itemToAdd = i % 2 ? sortedData[0] : sortedData[sortedData.length - 1];
      this.graphData.push(itemToAdd);
      sortedData.splice(sortedData.indexOf(itemToAdd), 1);
    }

    const radians = (angle: number) => angle * Math.PI / 180;

    this.total = this.graphData.reduce((total, curr) => total + curr.value, 0); // the total of all values
    let angleOffset = -90; // the running angle offset counter
    const paddingValue = (2 / this.strokeDasharray) * this.total;
    const totalWithPaddings = this.total + this.graphData.length * paddingValue;
    this.graphData.forEach((arc) => {
      // calculate and set the strokeOffset and angleOffsets for each item in array
      arc.strokeOffset = this.strokeDasharray - ((arc.value + paddingValue) / totalWithPaddings) * this.strokeDasharray;
      arc.angleOffset = angleOffset;

      // calculate the end cordinates of the line separator
      arc.line = {
        x: Math.cos(radians(angleOffset)) * this.height / 2,
        y: Math.sin(radians(angleOffset)) * this.height / 2
      };

      // update global angleOffset
      angleOffset += ((arc.value + paddingValue) / totalWithPaddings) * 360;

      // calculate the positions of labels
      const midAngle = (angleOffset + arc.angleOffset) / 2;
      arc.labelPosition = {
        left: midAngle < 90 ? (this.height / 2 + (Math.cos(radians(midAngle)) * (this.height + 25) / 2)) + 'px' : 'auto',
        right: midAngle > 90 ? (this.height / 2 - (Math.cos(radians(midAngle)) * (this.height + 25) / 2)) + 'px' : 'auto',
        top: (this.height / 2 + (Math.sin(radians(midAngle)) * (this.height + 25) / 2)) + 'px'
      };

    });
  }

}
