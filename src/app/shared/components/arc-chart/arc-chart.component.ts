import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'app-arc-chart',
  templateUrl: './arc-chart.component.html',
  styleUrls: ['./arc-chart.component.scss']
})
export class ArcChartComponent implements OnInit, OnChanges {

  @Input() value: number;
  @Input() total: number;
  @Input() color: string;
  @Input() backgroundColor: string;
  public height = 50;
  public strokeDasharray = 2 * Math.PI * (this.height / 2 - 2.5);
  public strokeOffset: number;
  public lineEndPoint: { x: number, y: number };

  constructor() { }

  ngOnInit() {
    this.setup();
  }

  ngOnChanges() {
    this.setup();
  }

  /**
   * setup
   * calculate the stroke offset required
   */
  private setup() {
    const paddingValue = 2;
    const fraction = (this.value + paddingValue) / (this.total + 2 * paddingValue);
    this.strokeOffset = this.strokeDasharray - fraction * this.strokeDasharray;

    const radians = (angle: number) => angle * Math.PI / 180;
    const angleOffset = -90 + fraction * 360;

    this.lineEndPoint = {
      x: Math.cos(radians(angleOffset)) * this.height / 2,
      y: Math.sin(radians(angleOffset)) * this.height / 2
    };
  }

}
