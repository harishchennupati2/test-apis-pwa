import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-status-banner',
  templateUrl: './status-banner.component.html',
  styleUrls: ['./status-banner.component.scss']
})
export class StatusBannerComponent implements OnInit {

  @Input() name: string;
  @Input() statuses: { value: number, label: string, color?: string }[];
  @Input() button: string;
  @Output() buttonClick = new EventEmitter<void>();
  public total: number;

  constructor(private utilService: UtilService) { }

  ngOnInit() {
    this.statuses.forEach(status => {
      status.color = this.utilService.getColorForStatus(status.label);
    });

    this.total = this.statuses.reduce((total, s) =>  total + s.value, 0);
  }

  public onButtonClick() {
    this.buttonClick.emit();
  }

}
