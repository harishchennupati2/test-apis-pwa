import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent {

  public on: boolean;
  // similarly to ngModel define a 2 way binding
  @Input()
  get appModel() {
    return this.on;
  }
  set appModel(val: boolean) {
    this.on = val;
  }
  @Output() appModelChange = new EventEmitter<boolean>();

  constructor() { }

  /**
   * toggle
   * change checked state and emit event
   */
  public toggle() {
    this.on = !this.on;
    this.appModelChange.emit(this.on);
  }

}
