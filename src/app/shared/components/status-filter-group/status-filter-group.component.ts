import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-status-filter-group',
  templateUrl: './status-filter-group.component.html',
  styleUrls: ['./status-filter-group.component.scss']
})
export class StatusFilterGroupComponent implements OnInit {

  @Input() statuses: string[];
  public colors: string[];
  public selectedOption: string;

  // like ngModel define a 2 way bidning for checked state
  @Input()
  get appModel() {
    return this.selectedOption;
  }
  set appModel(val: string) {
    this.selectedOption = val;
  }
  @Output() appModelChange = new EventEmitter<string>();
  @Output() change = new EventEmitter<void>();

  constructor(private utilService: UtilService) { }

  ngOnInit() {
    this.colors = this.statuses.map(status => this.utilService.getColorForStatus(status));
  }

  public onOptionSelected(option: string) {
    this.selectedOption = option;
    this.appModelChange.emit(option);
    this.change.emit();
  }

}
