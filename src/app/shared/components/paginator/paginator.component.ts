import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

/**
 * paginator component
 * simple pagination that outputs per-page and current-page values
 */
@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() total: number;
  @Output() change = new EventEmitter<{ perPage: number, pageNumber: number }>();
  private pageSizeOptions = [5, 10, 15, 20, 25];
  public pageOptions: number[];
  public prevDisabled: boolean;
  public nextDisabled: boolean;
  public page: number;
  public perPage: number;

  // page number two way binding
  @Input()
  get pageNumber() { return this.page; }
  set pageNumber(val: number) { this.page = val; }
  @Output() pageNumberChange = new EventEmitter<number>();

  // page size two way binding
  @Input()
  get pageSize() { return this.perPage; }
  set pageSize(val: number) { this.perPage = val; }
  @Output() pageSizeChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    this.update();
  }

  /**
   * component on changes
   * if there are changes to the total number of records,
   * then set the page back to 1
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.total) {
      this.update();
    }
  }

  /**
   * update
   * handles all the updates required for consistency
   */
  public update() {
    const maxPage = Math.ceil(this.total / this.perPage);
    if (this.page > maxPage) {
      return this.setPage(maxPage);
    }

    // the page options visible to user to click
    this.pageOptions = [];
    for (let i = 0; i < maxPage; i++) {
      this.pageOptions.push(i + 1);
    }
    if (this.pageOptions.length > 5) {
      if (this.page <= 3) {
        this.pageOptions = this.pageOptions.slice(0, 5);
      } else if (this.page >= maxPage - 2) {
        this.pageOptions = this.pageOptions.slice(this.pageOptions.length - 5);
      } else {
        this.pageOptions = this.pageOptions.slice(this.page - 3, this.page + 2);
      }
    }

    // can click on next / prev ?
    this.prevDisabled = this.page === 1;
    this.nextDisabled = this.page === maxPage;
  }

  public incrementPage(increment: number) {
    this.page += increment;
    this.update();
    this.pageNumberChange.emit(this.page);
    this.change.emit();
  }

  public setPage(page: number) {
    this.page = page;
    this.update();
    this.pageNumberChange.emit(this.page);
    this.change.emit();
  }

  public changePageSize(increment: number) {
    const currentIndex = this.pageSizeOptions.indexOf(this.perPage);
    if (this.pageSizeOptions[currentIndex + increment]) {
      this.perPage = this.pageSizeOptions[currentIndex + increment];
      this.update();
      this.pageSizeChange.emit(this.perPage);
      this.change.emit();
    }
  }

}
