import { Component, OnChanges, Input } from '@angular/core';

import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-details-panel',
  templateUrl: './details-panel.component.html',
  styleUrls: ['./details-panel.component.scss']
})
export class DetailsPanelComponent implements OnChanges {

  @Input() statuses: string[];
  @Input() currentStatus: string;
  @Input() label: string;
  @Input() id: string;

  public currentStatusIndex: number;
  public color: string;
  public icons: string[];

  constructor(private utilService: UtilService) { }

  ngOnChanges() {
    this.currentStatusIndex = this.statuses.indexOf(this.currentStatus);
    this.utilService.getColorForStatus(this.currentStatus);
    this.icons = this.statuses.map(status => this.utilService.getIconForStatus(status));
    this.color = this.utilService.getColorForStatus(this.currentStatus);
  }

  public isActive(status: string) {
    return status.toLowerCase() === this.currentStatus.toLowerCase();
  }

}
