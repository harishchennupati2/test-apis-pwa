import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * checkbox component
 */
@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {

  public checked: boolean;
  // like ngModel define a 2 way bidning for checked state
  @Input()
  get appModel() {
    return this.checked;
  }
  set appModel(val: boolean) {
    this.checked = val;
  }
  @Output() appModelChange = new EventEmitter<boolean>();
  @Output() change = new EventEmitter<void>();

  constructor() { }

  /**
   * on click
   * change state and emit event
   */
  public onClick() {
    this.checked = !this.checked;
    this.appModelChange.emit(this.checked);
    this.change.emit();
  }

}
