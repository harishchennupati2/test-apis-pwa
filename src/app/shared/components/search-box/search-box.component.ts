import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { SearchService } from 'src/app/core/services/search.service';

const ENTER_KEY_CODE = 13;

/**
 * search box
 * completely flexible search box
 * it allows parent component to bind to searchText (2-way), listen for changes on input, on focus
 * also can do the local search itself for the parent, if provided with a data list as Input() property
 */
@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnChanges {

  public searchText: string;
  public searchTextUpdate = new Subject<void>();
  @Input() placeholder: string;
  @Input() width = 250;
  @Input() blend: boolean;
  @Input() bordered: boolean;
  @Input() data: any[];
  @Output() filter = new EventEmitter<any[]>();
  @Output() focus = new EventEmitter<void>();

  // like ngModel define a 2 way bidning for checked state
  @Input()
  get appModel() {
    return this.searchText;
  }
  set appModel(val: string) {
    this.searchText = val;
    this.change.emit();
  }
  @Output() appModelChange = new EventEmitter<string>();
  @Output() change = new EventEmitter<void>();

  @Output() enter = new EventEmitter<string>();

  constructor(private searchService: SearchService) { }

  /**
   * component on changes
   * if changes are made to the data obj, then need to perform search again
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes.data) {
      // set up the debounced search flow
      this.searchTextUpdate
        .pipe(debounceTime(250))
        .subscribe(() => {
          this.filter.emit(this.searchService.filterItems(this.data, this.searchText));
        });
      this.searchTextUpdate.next();
    }
  }

  /**
   * clear the search
   */
  public clearSearch() {
    this.searchText = null;
    this.appModelChange.emit(this.searchText);
    this.change.emit();
    this.enter.emit(this.searchText);
    this.searchTextUpdate.next();
  }

  /**
   * host listener for keyboard events
   * interested in ENTER key
   */
  @HostListener('window:keydown', ['$event'])
  keyEvent(event: any) {
    if (event.keyCode !== ENTER_KEY_CODE) { return; }

    // notify parent that enter has been clicked
    this.enter.emit(this.searchText);

    event.stopPropagation();
    event.preventDefault();
  }

  /**
   * notify parent on input change
   */
  public onInput() {
    this.appModelChange.emit(this.searchText);
    this.searchTextUpdate.next();
  }

  /**
   * notify parent that search box has got focus
   */
  public onFocus() {
    this.focus.emit();
  }

}
