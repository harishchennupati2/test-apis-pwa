import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { WelcomeBannerComponent } from './components/welcome-banner/welcome-banner.component';
import { SummaryCardComponent } from './components/summary-card/summary-card.component';


@NgModule({
  declarations: [
    DashboardComponent, WelcomeBannerComponent, SummaryCardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class DashboardModule { }
