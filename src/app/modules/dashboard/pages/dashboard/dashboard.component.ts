import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Column } from 'src/app/core/models/column.model';
import { UserRole } from 'src/app/core/models/user-role.model';

import { OrderService } from 'src/app/core/http/order/order.service';
import { CoaService } from 'src/app/core/http/coa/coa.service';
import { InvoiceService } from 'src/app/core/http/invoice/invoice.service';
import { CoaByAgreementService } from 'src/app/core/http/coa/coa-by-agreement.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';
import { CustomerService } from 'src/app/core/http/customer/customer.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public cards = [
    {
      name: 'Order',
      columns: [
        { name: 'Order No.', key: 'orderNo', type: 'id' },
        { name: 'Order Type', key: 'orderType' },
        { name: 'Status', key: 'summarizedStatus', type: 'status' }
      ] as Column[],
      data: null,
      statuses: null,
      link: 'order-listing'
    },
    {
      name: 'COA',
      columns: [
        { name: 'COA ID', key: 'linearId', type: 'id' },
        { name: 'Customer PO Number', key: 'customerPoNumber', },
        { name: 'Status', key: 'customersigned', type: 'status' }
      ] as Column[],
      data: null,
      statuses: null,
      link: 'coas/coa-listing'
    },
    {
      name: 'COA by Agreement',
      columns: [
        { name: 'COA by Agreement ID', key: 'linearId', type: 'id' },
        { name: 'Schedule Number', key: 'schedulenumber' },
        { name: 'Status', key: 'signed', type: 'status' }
      ] as Column[],
      data: null,
      statuses: null,
      link: 'coas/coa-by-agreement-listing'
    },
    {
      name: 'Invoice',
      columns: [
        { name: 'Invoice No.', key: 'invoiceNo', type: 'id' },
        { name: 'Order Date', key: 'invoiceDate', type: 'date' },
        { name: 'Status', key: 'status', type: 'status' }
      ] as Column[],
      data: null,
      statuses: null,
      link: 'invoice-listing'
    }
  ];
  public showCustomersDropdown: boolean;
  public allCustomers: string[];
  public selectedCustomer: string;

  constructor(
    private orderService: OrderService,
    private coaService: CoaService,
    private coabyaService: CoaByAgreementService,
    private invoiceService: InvoiceService,
    private miscService: MiscellaneousService,
    private authService: AuthenticationService,
    private customerService: CustomerService,
    private router: Router
  ) { }

  ngOnInit() {
    const role = this.authService.getRole();
    if (role === UserRole.ManufacturingUnit || role === UserRole.InstallationPartner) {
      this.cards.splice(2);
    }
    this.showCustomersDropdown = role !== UserRole.EndCustomer;

    this.customerService.get().subscribe(customers => {
      this.allCustomers = customers.map(customer => customer.customer);
    });

    this.fetchData();
  }

  /**
   * on link click
   * @param card - the card in which the link was clicked (orders, invoice etc)
   * @param id - the id
   * navigate to the particular resource's details page
   */
  public onLinkClicked(card: any, id: string) {
    this.router.navigate([card.link + (id ? '/details/' + id : '')]);
  }

  public onCustomerSelected() {
    this.fetchData();
  }

  /**
   * fetch data
   * this is the card data and chart data for each card
   */
  private fetchData() {
    // reset current data to null, so that loaders are shown in cards
    this.cards.forEach(card => {
      card.data = null;
      card.statuses = null;
    });


    const role = this.authService.getRole();

    // get the statuses
    this.miscService.getStatuses().subscribe(res => {
      this.cards[0].statuses = res.order;
      this.cards[1].statuses = res.coa;
      // if role is ManufacturingUnit or InstallationPartner, only 2 cards are shown
      // as these roles are not allowed to view COA By Agreement and Invoice pages
      if (role !== UserRole.ManufacturingUnit && role !== UserRole.InstallationPartner) {
        this.cards[2].statuses = res.coabyagreement;
        this.cards[3].statuses = res.invoice;
      }
    });

    // common query params for all data requests
    const commonQueryParams = {
      pageSize: 5,
      pageNumber: 1,
      sortDirection: 'desc',
      sortColumn: 'purchaseOrderDate',
      customerName: this.selectedCustomer
    };

    // for each card / resource, get the data
    this.orderService.get(commonQueryParams).subscribe(res => {
      this.cards[0].data = res.pageItems;
    });
    this.coaService.get(commonQueryParams).subscribe(res => {
      this.cards[1].data = res.pageItems;
      res.pageItems.forEach(coa => coa.customersigned = coa.customersigned === 'yes' ? 'Signed' : 'Not Signed');
    });
    // if role is ManufacturingUnit or InstallationPartner, only 2 cards are shown
    // as these roles are not allowed to view COA By Agreement and Invoice pages
    if (role !== UserRole.ManufacturingUnit && role !== UserRole.InstallationPartner) {
      this.coabyaService.get(commonQueryParams).subscribe(res => {
        this.cards[2].data = res.pageItems;
        res.pageItems.forEach(coabya => coabya.signed = coabya.signed === 'yes' ? 'Signed' : 'Not Signed');
      });
      this.invoiceService.get(commonQueryParams).subscribe(res => {
        this.cards[3].data = res.pageItems;
      });
    }
  }

}
