import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Column } from 'src/app/core/models/column.model';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaService } from 'src/app/core/http/coa/coa.service';

@Component({
  selector: 'app-add-coas-modal',
  templateUrl: './add-coas-modal.component.html',
  styleUrls: ['./add-coas-modal.component.scss']
})
export class AddCoasModalComponent implements OnInit {

  constructor(
    public coaService: CoaService,
    public modal: NgbActiveModal
  ) { }

  public selectedCoas: Coa[];

  public columns = [
    { name: '', type: 'select', unsortable: true },
    { name: 'Serial No.', key: '' },
    { name: 'Customer PO No.', key: 'customerPoNumber' },
    { name: 'Customer Signed', key: 'customersigned' },
    { name: 'Customer Signed Date', key: 'customersigneddate', type: 'date' },
    { name: 'Customer Printed Name', key: 'customerprintedname' },
    { name: 'Signatory Position', key: 'signatoryposition' },
  ] as Column[];

  ngOnInit() {
  }

  public addItems() {
    this.modal.close(this.selectedCoas);
  }

}
