import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ClientConfig } from 'src/app/configs/client.config';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaByAgreement } from 'src/app/core/models/coa-by-agreement.model';

@Component({
  selector: 'app-customer-declaration',
  templateUrl: './customer-declaration.component.html',
  styleUrls: ['./customer-declaration.component.scss']
})
export class CustomerDeclarationComponent implements OnInit {

  @Input() coa: Coa | CoaByAgreement;
  @Input() type = 'coa';

  public signedDateKey: string;
  public printedNameKey: string;
  public signatoryPositionKey: string;

  @Output() signInit = new EventEmitter<void>();

  public instructions: string = ClientConfig.CONSIGNEE_INSTRUCTIONS.replace('<<CLIENT_NAME>>', ClientConfig.NAME);

  constructor() { }

  ngOnInit() {
    this.signedDateKey = this.type === 'coa' ? 'customersigneddate' : 'acceptancedate';
    this.printedNameKey = this.type === 'coa' ? 'customerprintedname' : 'printname';
    this.signatoryPositionKey = this.type === 'coa' ? 'signatoryposition' : 'title';
  }

  public initiateSign() {
    this.signInit.emit();
  }

}
