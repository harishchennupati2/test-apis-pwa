import { Component, OnInit, Input } from '@angular/core';

import { Address } from 'src/app/core/models/address.model';
import { Coa } from 'src/app/core/models/coa.model';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss']
})
export class AddressesComponent implements OnInit {

  @Input() coa: Coa;
  public addresses: { name: string, address: Address }[];

  constructor() { }

  ngOnInit() {
    this.addresses = [
      { name: 'Supplier Address', address: this.coa.supplieraddress },
      { name: 'Ship To', address: this.coa.shiptoaddress }
    ];
  }

}
