import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService as AuthGuard } from 'src/app/core/authentication/auth-guard.service';
import { UserRole } from 'src/app/core/models/user-role.model';

import { CoaListingComponent } from './pages/coa-listing/coa-listing.component';
import { CoaByAgreementListingComponent } from './pages/coa-by-agreement-listing/coa-by-agreement-listing.component';
import { CoaDetailsComponent } from './pages/coa-details/coa-details.component';
import { CoaByAgreementDetailsComponent } from './pages/coa-by-agreement-details/coa-by-agreement-details.component';
import { CreateNewByAgreementComponent } from './pages/create-new-by-agreement/create-new-by-agreement.component';

const routes: Routes = [
  {
    path: 'coa-listing',
    component: CoaListingComponent
  },
  {
    path: 'coa-listing/details/:id',
    component: CoaDetailsComponent
  },
  {
    path: 'coa-by-agreement-listing',
    component: CoaByAgreementListingComponent,
    canActivate: [AuthGuard],
    data: { roles: [UserRole.EndCustomer, UserRole.LeasingPartner, UserRole.SupplyChainTeam] }
  },
  {
    path: 'coa-by-agreement-listing/details/:id',
    component: CoaByAgreementDetailsComponent,
    canActivate: [AuthGuard],
    data: { roles: [UserRole.EndCustomer, UserRole.LeasingPartner, UserRole.SupplyChainTeam] }
  },
  {
    path: 'coa-by-agreement-listing/create-new',
    component: CreateNewByAgreementComponent,
    canActivate: [AuthGuard],
    data: { roles: [UserRole.LeasingPartner] }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoasRoutingModule { }
