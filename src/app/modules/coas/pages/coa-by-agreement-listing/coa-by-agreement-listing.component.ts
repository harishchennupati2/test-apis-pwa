import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserRole } from 'src/app/core/models/user-role.model';
import { Column } from 'src/app/core/models/column.model';
import { CoaByAgreement } from 'src/app/core/models/coa-by-agreement.model';
import { CoaByAgreementService } from 'src/app/core/http/coa/coa-by-agreement.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-coa-by-agreement-listing',
  templateUrl: './coa-by-agreement-listing.component.html',
  styleUrls: ['./coa-by-agreement-listing.component.scss']
})
export class CoaByAgreementListingComponent implements OnInit {

  public statusCounts: { value: number, label: string }[];
  public statuses = ['Signed', 'Not Signed'];
  public columns = [
    { name: 'COA By Agreement ID', key: 'linearId',  type: 'link' },
    { name: 'Master Agreement Number', key: 'masteragreementnumber' },
    { name: 'Schedule Number', key: 'schedulenumber' },
    { name: 'Leaser Org Name', key: 'leaserorgname' },
    { name: 'Equipment Location', key: 'equipmentlocation' },
    { name: 'Signed Status', key: 'signed' },
    { name: 'Printed Name', key: 'printname' },
    { name: 'Title', key: 'title' },
    { name: 'Status', key: 'signed', type: 'status' }
  ] as Column[];
  public userCanCreate: boolean;

  constructor(
    private miscService: MiscellaneousService,
    private router: Router,
    public coabyaService: CoaByAgreementService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    const role = this.authService.getRole();
    this.columns[this.columns.length - 1].editable = (item: CoaByAgreement) =>  {
      return role === UserRole.EndCustomer && item.signed === 'no';
    };
    this.userCanCreate = role === UserRole.LeasingPartner;

    this.miscService.getStatuses().subscribe(res => {
      this.statusCounts = res.coabyagreement;
    });
  }

  public navigateToCoabya(coabya: CoaByAgreement) {
    this.router.navigate(['/coas/coa-by-agreement-listing/details', coabya.linearId]);
  }

  public createNew() {
    this.router.navigate(['/coas/coa-by-agreement-listing/create-new']);
  }

}
