import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { UserRole } from 'src/app/core/models/user-role.model';
import { Column } from 'src/app/core/models/column.model';
import { Device } from 'src/app/core/models/device.model';
import { ClientConfig } from 'src/app/configs/client.config';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaService } from 'src/app/core/http/coa/coa.service';

@Component({
  selector: 'app-coa-details',
  templateUrl: './coa-details.component.html',
  styleUrls: ['./coa-details.component.scss']
})
export class CoaDetailsComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  public coa: Coa;
  public statuses = ['Not Signed', 'Signed'];
  public clientName = ClientConfig.NAME;
  public orderNumbers: string[];
  public devices: Device[];
  public filteredDevices: Device[];
  public signing: boolean;
  public userAllowedToSign: boolean;

  public deviceColumns = [
    { name: 'Asset No', key: 'assetNo' },
    { name: 'Product Description', key: 'productDescription' },
    { name: 'P/N', key: 'pn' },
    { name: 'Ordered Qty', key: 'orderedQty' },
    { name: 'Serial Number', key: 'serialNumber' },
    { name: 'Install Date', key: 'installDate', type: 'date' }
  ] as Column[];

  constructor(
    private coaService: CoaService,
    private route: ActivatedRoute,
    private location: Location,
    private authService: AuthenticationService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.userAllowedToSign = this.authService.getRole() === UserRole.EndCustomer;

    this.routeSubscription = this.route.params.subscribe(params => {
      this.coaService.getById(params.id).subscribe(coa => {
        this.coa = coa;
        this.orderNumbers = this.coa.orders.map(order => order.orderNo);

        this.devices = this.coa.orders
          .reduce((items, order) => {
            return items.concat(order.items.reduce((devices, item) => {
              return devices.concat(item.devices);
            }, []));
          }, []);
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  public goBack() {
    this.location.back();
  }

  public onSignInit() {
    const user = this.authService.getCurrentUser();
    this.coa.customerprintedname = user.username;
    this.coa.signatoryposition = user.signatoryposition;
    this.coa.customersigneddate = new Date();
  }

  public sign() {
    this.signing = true;
    this.coaService
      .sign(this.coa.linearId, this.coa.customerprintedname, this.coa.signatoryposition)
      .subscribe(coa => {
        this.signing = false;
        this.coa = coa;
        this.toastService.show({ message: 'COA has been signed!', color: 'success' });
      });
  }

}
