import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';

import { AddCoasModalComponent } from '../../components/add-coas-modal/add-coas-modal.component';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { CoaByAgreementService } from 'src/app/core/http/coa/coa-by-agreement.service';

import { ClientConfig } from 'src/app/configs/client.config';
import { User } from 'src/app/core/models/user.model';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaByAgreement } from 'src/app/core/models/coa-by-agreement.model';

import { ToastService } from 'src/app/core/services/toast.service';

@Component({
  selector: 'app-create-new-by-agreement',
  templateUrl: './create-new-by-agreement.component.html',
  styleUrls: ['./create-new-by-agreement.component.scss']
})
export class CreateNewByAgreementComponent implements OnInit {

  public statuses = ['Not Signed', 'Signed'];
  public coas: Coa[] = [];
  public filteredCoas: Coa[];
  public coabya: CoaByAgreement;
  public today = new Date();
  public user: User;
  public creating: boolean;
  public showInputErrors: boolean;
  public orderNumber: string;

  constructor(
    private authService: AuthenticationService,
    private modalService: NgbModal,
    private coabyaService: CoaByAgreementService,
    private toastService: ToastService,
    private location: Location
  ) { }

  ngOnInit() {
    this.user = this.authService.getCurrentUser();

    this.coabya = new CoaByAgreement();
    this.coabya.leaserorgname = ClientConfig.NAME;
    this.coabya.customerorgname = this.user.customerorgname;
    this.coabya.coas = [];
  }

  public goBack() {
    this.location.back();
  }

  public addCoas() {
    const modalRef = this.modalService.open(AddCoasModalComponent, { size: 'lg' });
    modalRef.result.then((addedItems: Coa[]) => {
      if (addedItems && addedItems.length) {
        this.coabya.coas = this.coabya.coas || [];
        this.coabya.coas = this.coabya.coas.concat(
          addedItems.filter(coa => {
            return this.coabya.coas.map(c => c.linearId).indexOf(coa.linearId) === -1;
          })
        );
      }
    }).catch(() => {});
  }

  public removeCoa(coa: Coa) {
    this.coabya.coas = this.coabya.coas.filter(c => c !== coa);
  }

  public create() {
    this.showInputErrors = true;
    if (!(this.orderNumber && this.coabya.masteragreementnumber && this.coabya.schedulenumber && this.coabya.equipmentlocation)) { return; }
    this.creating = true;
    this.coabyaService.create(this.coabya).subscribe(() => {
      this.toastService.show({ message: 'Successfully created new COA by Agreement!', color: 'success' });
      this.goBack();
    });
  }

}
