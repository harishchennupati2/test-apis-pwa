import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { cloneDeep, some } from 'lodash';

import { LocalStorageConfig } from 'src/app/configs/local-storage.config';
import { Column } from 'src/app/core/models/column.model';
import { Device } from 'src/app/core/models/device.model';
import { OrderItem } from 'src/app/core/models/order-item.model';
import { UtilService } from 'src/app/core/services/util.service';
import { UserRole } from 'src/app/core/models/user-role.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

// a new type for devices that could be selected
type DeviceS = Device & { selected?: boolean };

@Component({
  selector: 'app-items-table',
  templateUrl: './items-table.component.html',
  styleUrls: ['./items-table.component.scss']
})
export class ItemsTableComponent implements OnInit, OnChanges {

  @Input() items: OrderItem[];
  @Input() allColumnsRef: Column[];

  @Output() updateDeviceStatuses = new EventEmitter<any[]>();

  // the columsn for the main outer table (these are properties on OrderItem level)
  public mainColumns = [
    { name: '', type: 'expand' },
    { name: 'Sequence No.', type: 'serial' },
    { name: 'Item Number', key: 'itemNumber' },
    { name: 'Item Number SO', key: 'itemNumberSo' },
    { name: 'Item Number Customer', key: 'itemNumberCustomer' },
    { name: 'Product No.', key: 'productNo' },
    { name: 'Customer Product No.', key: 'customerProductNo' },
    { name: 'Mfg. Product No.', key: 'mfgProductNo' },
    { name: 'Product Description', key: 'productDescription' },
    { name: 'Product Line Code', key: 'productLineCode' },
    { name: 'Is Tangible', key: 'isTangible', type: 'boolean' },
    { name: 'Is Carepack', key: 'isCarePack', type: 'boolean' },
    { name: 'Is Child', key: 'isChild', type: 'boolean' },
    { name: 'Parent Number', key: 'parentNumber' },
    { name: 'Bundle ID', key: 'bundleID' },
    { name: 'Bundle Name', key: 'bundleName' },
    { name: 'Item Currency', key: 'itemCurrency' },
    { name: 'Net Line Price', key: 'netLinePrice', type: 'number' },
    { name: 'Net Line Price USD', key: 'netLinePriceUSD', type: 'number' },
    { name: 'Bundle Price', key: 'bundlePrice', type: 'number'},
    { name: 'Bundle Price USD', key: 'bundlePriceUSD', type: 'number' },
    { name: 'Order Qty', key: 'orderedQty', type: 'number' },
    { name: 'Shipped Qty', key: 'shippedQty', type: 'number' },
    { name: 'Deliverd Qty', key: 'deliveredQty', type: 'number' },
    { name: 'Planned Shipment Date', key: 'plannedShipDate', type: 'time' },
    { name: 'Planned Delivery  Date', key: 'plannedDeliveryDate', type: 'time' },
    { name: 'Actual Delivery Date', key: 'actualDeliveryDate', type: 'time' },
    { name: 'Status', key: 'itemStatus', type: 'status' }
  ] as Column[];

  // the columns for the carepack table inside each item
  public carepackColumns = [
    { name: '', type: 'expand' },
    { name: 'Carepack Certificate Number', key: 'certificateNumber' },
    { name: 'Carepack Related Product Number', key: 'relatedProductNumber' },
    { name: 'Carepack Related Serial Number', key: 'relatedSerialNumber' }
  ] as Column[];

  // the columns for the devices table inside each item
  public deviceColumns = [
    { name: 'Device Sno', type: 'serial' },
    { name: 'Asset Tag', key: 'assetTag' },
    { name: 'Serial Number', key: 'serialNumber' },
    { name: 'Box No.', key: 'boxNo' },
    { name: 'UUID', key: 'uuid' },
    { name: 'Primary MAC Address', key: 'primaryMacAddress' },
    { name: 'BIOS Version', key: 'biosVersion' },
    { name: 'Image Version', key: 'imageVersion' },
    { name: 'LAN MAC Address', key: 'lanMacAddress' },
    { name: 'W-LAN MAC Address', key: 'wlanMacAddress' },
    { name: 'Secondary MAC Address 1', key: 'secondaryMacAddress1' },
    { name: 'Secndary MAC Address 2', key: 'secondaryMacAddress2' },
    { name: 'Status', key: 'status', type: 'status' }
  ] as Column[];

  public statuses: { label: string, color: string }[];
  public userCanEditStatus: boolean;

  public deviceStatuses = ['Installed', 'Delivered', 'Installing'];
  public devices: { [itemNum: string]: { [status: string]: DeviceS[] }} = {};

  public selectedDeviceStatus: { [itemNum: string]: { [status: string]: string }} = {};

  constructor(private utilService: UtilService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.userCanEditStatus = this.authService.getRole() === UserRole.InstallationPartner;

    this.statuses = ['Open', 'In-Production', 'Delivered', 'Installed', 'Closed']
      .map(s => ({ label: s, color: this.utilService.getColorForStatus(s) }));

    // column display settings are stored in localstorage
    const savedSettings: Column[] = JSON.parse(localStorage.getItem(LocalStorageConfig.ORDER_ITEM_COLUMN_SETTINGS)) || [];
    // for each category of columns (orderItem, device, carepack), create a map of settings by column name
    const savedMap = {};
    savedSettings.forEach(column => {
      savedMap[column.category] = savedMap[column.category] || {};
      savedMap[column.category][column.name] = column.hidden; // the column visibility
    });

    // for each category of columns, look at all columsn, and refer to the map above, for display setting
    ['main', 'carepack', 'device'].forEach(category => {
      (this[category + 'Columns'] as Column[]).forEach(column => {
        column.category = category;
        // column is marked hidden or not
        column.hidden = savedMap[category] ? savedMap[category][column.name] : false;
      });
    });

    // This array reference is used by the parent when opening Manage Columns Sidebar
    const allColumns = this.mainColumns.concat(this.carepackColumns).concat(this.deviceColumns);
    // add all columns in place into the referenceArray input
    allColumns.forEach(column => {
      this.allColumnsRef.push(column);
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.items) { return; }
    if (!this.items) { return this.devices = {}; }

    this.devices = {};
    this.selectedDeviceStatus = {};
    /**
     * we map the devices per item and then per device status
     * so it looks like this
     * devices = {
     *  [itemNo]: {
     *    [deviceStatus]: Device[]
     *  }
     * }
     * and similar to maintain the status dropdown for batch status updates
     */
    this.items.forEach(item => {
      const itemDevices = item.carepackDetails.reduce((devices, carepack) => devices.concat(carepack.devices), []);

      this.selectedDeviceStatus[item.itemNumber] = {};
      this.devices[item.itemNumber] = this.deviceStatuses.reduce((accu, status) => {
        this.selectedDeviceStatus[item.itemNumber][status] = undefined;

        accu[status] = cloneDeep(itemDevices).filter((device: Device) => device.status.toLowerCase() === status.toLowerCase());
        return accu;
      }, {});
    });
  }

  /**
   * get visibility count
   * how many columns visible
   * @params columns - the list of columns to check in
   * used to set the colspan attribute of expansion cells
   */
  public getVisibleCount(columns: Column[]) {
    return columns.filter(column => !column.hidden).length;
  }

  /**
   * returns true if all the devices in the active tab are selected
   * @param devices - devices in the active tab for an item
   */
  public allDevicesSelected(devices: DeviceS[]) {
    return devices.length > 0 && devices.filter(d => !d.selected).length === 0;
  }

  /**
   * returns true if any device is selected
   * @param devices - devices in the active tab for an item
   */
  public anyDeviceSelected(devices: DeviceS[]) {
    return some(devices, { selected: true });
  }

  /**
   * toggles the selection of all devices in an item
   * @param event - checkbox event
   * @param devices - devices in the active tab for an item
   */
  public toggleDevices(event: boolean, devices: DeviceS[]) {
    devices.forEach(device => device.selected = event);
  }

  /**
   *  update order devices per item
   *  as of now it handles only updating device status
   */
  public onUpdateOrderDevices(itemNo: string) {
    const devicesByStatus = this.devices[itemNo];
    let devices = [];

    // we first check the inline status of the device
    // and override it with the tab's selected status if the device is checked
    this.deviceStatuses.forEach(deviceStatus => {
      const newStatus = this.selectedDeviceStatus[itemNo][deviceStatus];
      devices = devicesByStatus[status].reduce((accu, device) => {
        const { serialNumber, installDate, status, selected } = device;
        return accu.concat({
          serialNo: serialNumber,
          installDate,
          status: (selected && newStatus ? newStatus : status).toUpperCase()
        });
      }, devices);
    });

    this.updateDeviceStatuses.emit([{ itemNo, devices }]);
  }

}
