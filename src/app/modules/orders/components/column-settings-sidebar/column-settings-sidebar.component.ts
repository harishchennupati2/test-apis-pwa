import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Column } from 'src/app/core/models/column.model';
import { LocalStorageConfig } from 'src/app/configs/local-storage.config';


@Component({
  selector: 'app-column-settings-sidebar',
  templateUrl: './column-settings-sidebar.component.html',
  styleUrls: ['./column-settings-sidebar.component.scss']
})
export class ColumnSettingsSidebarComponent implements OnInit {

  @Input() columns: Column[];
  @Output() close = new EventEmitter<void>();
  public toggleList: boolean[];

  constructor() { }

  ngOnInit() {
    this.columns = this.columns.filter(column => column.name);
    this.toggleList = this.columns.map(column => !column.hidden);
  }

  public onClose() {
    this.close.emit();
  }

  /**
   * apply
   * set the visibility in the column metata
   * save the settings to local stoarge and close
   */
  public apply() {
    this.columns.forEach((column, index) => {
      column.hidden = !this.toggleList[index];
    });

    localStorage.setItem(LocalStorageConfig.ORDER_ITEM_COLUMN_SETTINGS, JSON.stringify(this.columns));
    this.onClose();
  }

}
