import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrderListingComponent } from './pages/order-listing/order-listing.component';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';
import { ItemsTableComponent } from './components/items-table/items-table.component';
import { ColumnSettingsSidebarComponent } from './components/column-settings-sidebar/column-settings-sidebar.component';

@NgModule({
  declarations: [
    OrderListingComponent,
    OrderDetailsComponent,
    ItemsTableComponent,
    ColumnSettingsSidebarComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule
  ]
})
export class OrdersModule { }
