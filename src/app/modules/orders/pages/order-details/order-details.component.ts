import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

import { Order } from 'src/app/core/models/order.model';
import { OrderItem } from 'src/app/core/models/order-item.model';
import { Address } from 'src/app/core/models/address.model';
import { OrderService } from 'src/app/core/http/order/order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  public order: Order;
  public filteredOrderItems: OrderItem[];
  public statuses = ['Open', 'In-Production', 'Delivered', 'Closed'];
  public orderId: string;

  public detailFields = [
    { name: 'OS', key: 'os' },
    { name: 'Customer Base No.', key: 'customerBaseNo' },
    { name: 'Cust. Purchase Agent Email', key: 'custPurchaseAgentEmail' },
    { name: 'Order No.', key: 'orderNo' },
    { name: 'Customer Name', key: 'customerName' },
    { name: 'Quote Number', key: 'quoteNumber' },
    { name: 'Sales Order No.', key: 'salesOrderNo' },
    { name: 'Purchase Order No.', key: 'purchaseOrderNumber' },
    { name: 'Shipment ID', key: '' },
    { name: 'Order Type', key: 'orderType' },
    { name: 'Summarized Status', key: 'summarizedStatus' },
    { name: 'Tracking Number', key: '' },
    { name: 'Order Type Description', key: 'orderTypeDescription' },
    { name: 'Invoice Status', key: 'invoiceStatus' },
    { name: 'Carrier', key: '' },
    { name: 'Purchase Order Date', key: 'purchaseOrderDate', type: 'time' },
    { name: 'Sales Rep.', key: 'hpSalesRep' },
    { name: 'Expected Delivery Date', key: '', type: 'time' },
    { name: 'Purchase Agreement', key: 'purchaseAgreementNo' },
    { name: 'Cust. Purchase Agent Name', key: 'custPurchaseAgentName' }
  ];

  public allItemColumns = [];
  public showColumnSettingsSidebar: boolean;
  public addresses: { name: string, address: Address }[];

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.orderId = params.id;
      this.getOrderById();
    });
  }

  public getOrderById() {
    this.orderService.getByOrderNo(this.orderId).subscribe(order => {
      this.order = order;
      this.addresses = [
        { name: 'Sold To', address: order.soldTo },
        { name: 'Ship To', address: order.shipTo },
        { name: 'Invoice To', address: order.invoiceTo }
      ];
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  public goBack() {
    this.location.back();
  }

  public onUpdateOrderDevices(items: OrderItem[]) {
    const { linearId, orderNo } = this.order;
    this.order = undefined;
    this.orderService.updateOrderDevices([{ linearId, orderNo, items }])
      .subscribe(() => this.getOrderById());
  }

}
