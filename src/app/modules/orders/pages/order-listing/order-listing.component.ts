import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Column } from 'src/app/core/models/column.model';
import { Order } from 'src/app/core/models/order.model';
import { OrderService } from 'src/app/core/http/order/order.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';

@Component({
  selector: 'app-order-listing',
  templateUrl: './order-listing.component.html',
  styleUrls: ['./order-listing.component.scss']
})
export class OrderListingComponent implements OnInit {

  public statusCounts: { value: number, label: string }[];
  public statuses = ['Open', 'In-Production', 'Delivered', 'Closed'];
  public columns = [
    { name: 'OS', key: 'os' },
    { name: 'Order No.', key: 'orderNo', type: 'link' },
    { name: 'Sales Order No.', key: 'salesOrderNo' },
    { name: 'Order Type', key: 'orderType' },
    { name: 'Order Type Desc.', key: 'orderTypeDescription' },
    { name: 'Purchase Order Date', key: 'purchaseOrderDate', type: 'time' },
    { name: 'Purchase Agreement No.', key: 'purchaseAgreementNo' },
    { name: 'PO. No.', key: 'purchaseOrderNumber' },
    { name: 'Quote Number', key: 'quoteNumber' },
    { name: 'Summarized Status', key: 'summarizedStatus', type: 'status' }
  ] as Column[];

  constructor(
    private miscService: MiscellaneousService,
    private router: Router,
    public orderService: OrderService
  ) { }

  ngOnInit() {
    this.miscService.getStatuses().subscribe(res => {
      this.statusCounts = res.order;
    });
  }

  public navigateToOrder(order: Order) {
    this.router.navigate(['/order-listing/details', order.orderNo]);
  }

}
