import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { UtilService } from 'src/app/core/services/util.service';
import { User } from 'src/app/core/models/user.model';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {

  public user: User;
  public timeOfDay: string;
  public role: string;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    this.user = this.authService.getCurrentUser();
    this.role = this.utilService.camelCaseToCapitalCase(this.user.role);
    this.timeOfDay = this.utilService.getTimeOfDay();

    setTimeout(() => {
      this.router.navigate(['/dashboard']);
    }, 1500);

  }

}
