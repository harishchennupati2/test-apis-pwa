import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService as AuthGuard } from 'src/app/core/authentication/auth-guard.service';
import { UserRole } from 'src/app/core/models/user-role.model';

import { InvoiceListingComponent } from './pages/invoice-listing/invoice-listing.component';
import { InvoiceDetailsComponent } from './pages/invoice-details/invoice-details.component';
import { CreateNewComponent } from './pages/create-new/create-new.component';

const routes: Routes = [
  {
    path: '',
    component: InvoiceListingComponent
  },
  {
    path: 'details/:id',
    component: InvoiceDetailsComponent
  },
  {
    path: 'create-new/:id',
    component: CreateNewComponent,
    canActivate: [AuthGuard],
    data: { roles: [UserRole.SupplyChainTeam] }
  },
  {
    path: '**',
    redirectTo: '',
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
