import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { Column } from 'src/app/core/models/column.model';
import { InvoiceItem } from 'src/app/core/models/invoice-item.model';

@Component({
  selector: 'app-items-table',
  templateUrl: './items-table.component.html',
  styleUrls: ['./items-table.component.scss']
})
export class ItemsTableComponent implements OnInit, OnChanges {

  @Input() items: InvoiceItem[];
  @Input() deletable: boolean;
  @Output() delete = new EventEmitter<InvoiceItem>();

  public columns = [
    { name: 'Customer Item No', key: 'itemNumberCustomer' },
    { name: 'Product Description', key: 'productDescription' },
    { name: 'Product Number', key: 'productNumber' },
    { name: 'Serial Number', key: 'serialNo' },
    { name: 'Net Unit Price (GBP)', key: 'unitPrice', type: 'number' },
    { name: 'Tax Amount', key: 'taxAmount', type: 'percentage' },
    { name: 'Qty.', key: 'quantity', type: 'number' },
    { name: 'Total Net Price (GBP)', key: 'totalNetPrice', type: 'number', style: 'right-align' },
    { name: 'Net Price With Tax (GBP)', key: 'netPriceWithTax', type: 'number', style: 'right-align' }
  ] as Column[];

  public totalNetPrice: number;
  public netPriceWithTax: number;

  constructor() { }

  ngOnInit() {
    if (this.deletable) {
      this.columns.push({ name: '', type: 'delete', style: 'delete' });
    }
  }

  ngOnChanges() {
    if (!this.items) { return; }
    this.totalNetPrice = this.items.reduce((total, item) => total + parseFloat(item.totalNetPrice), 0);
    this.netPriceWithTax = this.items.reduce((total, item) => total + parseFloat(item.netPriceWithTax), 0);
  }

  public onDelete(item: InvoiceItem) {
    this.delete.emit(item);
  }

}
