import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { groupBy } from 'lodash';

import { AddItemsModalComponent } from '../../components/add-items-modal/add-items-modal.component';

import { Invoice } from 'src/app/core/models/invoice.model';
import { InvoiceItem } from 'src/app/core/models/invoice-item.model';
import { InvoiceService } from 'src/app/core/http/invoice/invoice.service';
import { UtilService } from 'src/app/core/services/util.service';
import { ToastService } from 'src/app/core/services/toast.service';


@Component({
  selector: 'app-create-new',
  templateUrl: './create-new.component.html',
  styleUrls: ['./create-new.component.scss']
})
export class CreateNewComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  public invoice: Invoice;
  public filteredItems: InvoiceItem[];

  public staticPurchaseOrderNumbersText: string[];
  public purchaseOrders: string[];

  constructor(
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    private location: Location,
    public utilService: UtilService,
    private toastService: ToastService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.invoiceService.getByInvoiceNo(params.id).subscribe(invoice => {
        this.invoice = invoice;
        this.staticPurchaseOrderNumbersText = this.getPurchaseOrders();
        this.purchaseOrders = this.getPurchaseOrders();
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  public goBack() {
    this.location.back();
  }

  /**
   * get purchase orders
   * compile the list of purchase orders from the current items in invoice
   */
  private getPurchaseOrders() {
    const orderNosMap = {};
    this.invoice.invoicedItems.forEach(item => orderNosMap[item.orderNo] = true);
    return Object.keys(orderNosMap);
  }

  public onRemoveItem(item: InvoiceItem) {
    this.invoice.invoicedItems = this.invoice.invoicedItems.filter(i => i !== item);
    // update the purchase orders list
    this.purchaseOrders = this.getPurchaseOrders();
  }

  /**
   * remove purchase order
   * remove all items from invoice with the given orderNo
   * @param order - the orderNo
   */
  public removePurchaseOrder(order: string) {
    this.invoice.invoicedItems = this.invoice.invoicedItems.filter(i => i.orderNo !== order);
    this.purchaseOrders = this.getPurchaseOrders();
  }

  /**
   * open add items modal
   */
  public addItems() {
    const modalRef = this.modalService.open(AddItemsModalComponent, { size: 'lg' });
    // if modal returns a list of items, add those to the invoice
    modalRef.result.then((addedItems: InvoiceItem[]) => {
      if (addedItems && addedItems.length) {
        this.invoice.invoicedItems = this.invoice.invoicedItems.concat(
          // only add the items that are not already present in the invoice
          addedItems.filter(item => {
            return this.invoice.invoicedItems.map(i => i.serialNo).indexOf(item.serialNo) === -1;
          })
        );
        this.purchaseOrders = this.getPurchaseOrders();
      }
    }).catch(() => {});
  }

  /**
   * create invoice from a draft invoice
   */
  public createInvoice() {
    const { linearId, invoicedItems } = this.invoice;
    const groupedInvoiceItems = groupBy(invoicedItems, 'orderNo');

    const orders = this.purchaseOrders.map(orderNo => ({
      linearId,
      orderNo,
      items: groupedInvoiceItems[orderNo].map((item: InvoiceItem) => {
        const { itemNumberCustomer, unitPrice, quantity, taxAmount, devices } = item;
        return {
          unitPrice,
          quantity,
          taxAmount,
          itemNumberCustomer,
          devices: devices.map(d =>  ({ serialNo: d.serialNumber }))
        };
      })
    }));

    this.invoiceService.create(linearId, { orders }).subscribe(() => {
      this.toastService.show({ message: 'Successfully created new Invoice!', color: 'success' });
      this.goBack();
    });
  }

}
