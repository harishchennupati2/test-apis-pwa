import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

import { UtilService } from 'src/app/core/services/util.service';

import { Invoice } from 'src/app/core/models/invoice.model';
import { InvoiceItem } from 'src/app/core/models/invoice-item.model';
import { InvoiceService } from 'src/app/core/http/invoice/invoice.service';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss']
})
export class InvoiceDetailsComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  public invoice: Invoice;
  public filteredItems: InvoiceItem[];
  public statuses = ['Draft', 'Active'];

  public purchaseOrderNumbers: string[];

  constructor(
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    private location: Location,
    public utilService: UtilService
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.invoiceService.getByInvoiceNo(params.id).subscribe(invoice => {
        this.invoice = invoice;
        // compile the purchaseOrderNumbers string
        // each invoice hsa multiple items from separate orders
        const orderNosMap = {};
        invoice.invoicedItems.forEach(item => orderNosMap[item.orderNo] = true);
        this.purchaseOrderNumbers = Object.keys(orderNosMap);
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  public goBack() {
    this.location.back();
  }

}
