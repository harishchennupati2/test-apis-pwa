import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoiceListingComponent } from './pages/invoice-listing/invoice-listing.component';
import { InvoiceDetailsComponent } from './pages/invoice-details/invoice-details.component';
import { ItemsTableComponent } from './components/items-table/items-table.component';
import { CreateNewComponent } from './pages/create-new/create-new.component';
import { AddItemsModalComponent } from './components/add-items-modal/add-items-modal.component';


@NgModule({
  declarations: [InvoiceListingComponent, InvoiceDetailsComponent, ItemsTableComponent, CreateNewComponent, AddItemsModalComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    SharedModule,
    NgbModule
  ],
  entryComponents: [
    AddItemsModalComponent
  ]
})
export class InvoicesModule { }
