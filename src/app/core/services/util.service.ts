import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  private statusColors = [
    { label: 'open', color: 'balanced' },
    { label: 'closed', color: 'warning' },
    { label: 'delivered', color: 'success' },
    { label: 'in-production', color: 'positive' },
    { label: 'installed', color: 'installed' },
    { label: 'not signed', color: 'danger' },
    { label: 'signed', color: 'success' },
    { label: 'active', color: 'positive' },
    { label: 'draft', color: 'warning' }
  ];

  private statusIcons = [
    { label: 'open', color: 'assignment_returned' },
    { label: 'closed', color: 'assignment_turned_in' },
    { label: 'delivered', color: 'assignment' },
    { label: 'in-production', color: 'assignment' },
    { label: 'not signed', color: 'assignment_late' },
    { label: 'signed', color: 'assignment_turned_in' },
    { label: 'active', color: 'check_box' },
    { label: 'draft', color: 'indeterminate_check_box' }
  ];

  constructor() { }

  /**
   * get color for status
   * from the status colors map,
   * @returns string - the themed color naem for the given status
   */
  public getColorForStatus(status: string): string {
    if (!status) { return null; }
    const index = this.statusColors
      .map(s => s.label)
      .indexOf(this.camelCaseToCapitalCase(status).toLowerCase());
    if (index === -1) { return null; }
    return this.statusColors[index].color;
  }

  /**
   * get icon for status
   * from the status icons map,
   * @returns string - the name of the icon for given status
   */
  public getIconForStatus(status: string): string {
    if (!status) { return null; }
    const index = this.statusIcons
      .map(s => s.label)
      .indexOf(this.camelCaseToCapitalCase(status).toLowerCase());
    if (index === -1) { return null; }
    return this.statusIcons[index].color;
  }

  /**
   * get human readable status text
   * @returns string - the themed color naem for the given status
   */
  public getStatusDisplayText(status: string): string {
    if (!status) { return null; }
    if (status.toLowerCase() === 'yes') {
      status = 'Signed';
    }
    if (status.toLowerCase() === 'no') {
      status = 'Not Signed';
    }
    return this.camelCaseToCapitalCase(status);
  }


  /**
   * get time of day
   * @returns string - (i.e. 'morning', 'evening', 'night' etc)
   */
  public getTimeOfDay(): string {
    const hours = new Date().getHours();
    const mat = [
      [0, 4, 'night'],
      [5, 11, 'morning'],
      [12, 19, 'evening'],
      [20, 24, 'night']
    ];
    return mat.filter(slot => hours >= slot[0] &&  hours <= slot[1])[0][2] as string;
  }

  /**
   * make texts like 'NotSigned' or 'EndCustomer' as 'Not Signed' and 'End Customer'
   */
  public camelCaseToCapitalCase(value: string) {
    if (value.match(/[A-Z]/g) && value.match(/[a-z]/g)) {
      value = value.replace(/([^\-])(\s*[A-Z])/g, '$1 $2').trim();
    }
    return value.replace(/\s+/g, ' ');
  }

}
