import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserRole } from '../../models/user-role.model';
import { ApiService } from '../api.service';

import { AuthenticationService } from '../../authentication/authentication.service';
import { UtilService } from '../../services/util.service';

@Injectable({
  providedIn: 'root'
})
export class MiscellaneousService {

  constructor(
    private api: ApiService,
    private authService: AuthenticationService,
    private utilService: UtilService
  ) { }

  /**
   * calls the status api
   * @params - any optional params
   * transforms the returned response to a more consitent one
   */
  public getStatuses(params?: any): Observable<{[key: string]: {value: number, label: string}[]}> {
    const role = this.authService.getRole();
    if (role === UserRole.EndCustomer) {
      const user = this.authService.getCurrentUser();
      params = Object.assign({}, params, { customerName: user.customerorgname });
    }
    return this.api
      .get('statuses/counts', { params })
      .pipe(map(res => {
        const transform = (s: any): { value: number, label: string } => {
          let label = s.summarizedStatus || s.status;
          label = this.utilService.getStatusDisplayText(label);
          label = label.toLowerCase();
          return { value: s.count, label };
        };

        res.order = res.order.map(transform);
        res.coa = res.coa.map(transform);
        res.coabyagreement = res.coabyagreement.map(transform);
        res.invoice = res.invoice.map(transform);

        // if user role is EndCustomer or LeasingPartner, only ACTIVE stats are shown
        // but since API returns all statuses, we modify the object here
        if (role === UserRole.EndCustomer || role === UserRole.LeasingPartner) {
          const totalNonActive = res.invoice
            .filter((status: any) => status.label.toLowerCase() !== 'active')
            .reduce((total: number, status: any) => total + status.value, 0);
          res.invoice = res.invoice.filter((status: any) => status.label.toLowerCase() === 'active');
          res.invoice.push({ value: totalNonActive });
        }

        return res;
      }));
  }

  /**
   * search for any resource by ID
   * @param text the text for id search
   */
  public search(text: string) {
    const params = { pageNumber: 1, pageSize: 4 };
    const role = this.authService.getRole();
    return this.api
      .get(`any/${text}`, { params })
      .pipe(map((res: any[]) => {
        if (role === UserRole.InstallationPartner || role === UserRole.ManufacturingUnit) {
          res = res.filter(entity => entity.entityType === 'orders' || entity.entityType === 'coas');
        }
        return res;
      }));
  }

}
