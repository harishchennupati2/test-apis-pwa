import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { get } from 'lodash';

import { InvoiceItem } from '../../models/invoice-item.model';
import { OrderItem } from '../../models/order-item.model';
import { OrderService } from '../order/order.service';
import { ApiService } from '../api.service';


@Injectable({
  providedIn: 'root'
})
export class InvoiceItemService {

  constructor(private api: ApiService) { }

  /**
   * get paginated invoice Items
   * This method wraps around the Orders API
   * and returns the extracted items in the format of InvoiceItem class
   * as such pagination is not going to work properly here, as pagination is controlled by the Orders API
   * and each order can have many Items
   */
  public get(params?: any): Observable<{ total: number, pageItems: InvoiceItem[] }> {
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);

    return this.api.get('orders/items/devices/signed', { params, observe: 'response' }).pipe(map(res => {
      res.body.forEach(order => order.items.forEach(item => item.orderNo = order.orderNo));
      const items = res.body.reduce((itemList, order) => itemList.concat(order.items), []);
      const invoiceItems: InvoiceItem[] = items.map((item: OrderItem) => {
        // transform the OrderItem objects to InvoiceItem format
        const invoicedItem = new InvoiceItem();
        invoicedItem.orderNo = item.orderNo;
        invoicedItem.itemNumberCustomer = item.itemNumberCustomer;
        invoicedItem.productDescription = item.productDescription;
        invoicedItem.productNumber = item.productNo;
        invoicedItem.devices = item.carepackDetails
          ? item.carepackDetails.reduce((accu, carepack) => accu.concat(carepack.devices), []) : [];
        invoicedItem.serialNo = get(invoicedItem.devices, '[0].serialNumber', '');
        invoicedItem.unitPrice = item.netLinePrice;
        invoicedItem.quantity = item.orderedQty;
        invoicedItem.totalNetPrice = String(parseFloat(item.netLinePrice) * parseFloat(item.orderedQty));
        return invoicedItem;
      });
      return {
        total: res.headers.get('X-Total-Count'),
        pageItems: invoiceItems
      };
    }));
  }

}
