import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Invoice } from '../../models/invoice.model';
import { UserRole } from '../../models/user-role.model';
import { ApiService } from '../api.service';

import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private api: ApiService, private authService: AuthenticationService) { }

  /**
   * get paginated invoices
   * @param params the query params
   * @param suffix the path suffix to add
   */
  public get(params?: any, suffix?: string): Observable<{ total: number, pageItems: Invoice[] }> {
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);

    // if role is EndCustomer, customer name param is mandatory
    const role = this.authService.getRole();
    if (role === UserRole.EndCustomer) {
      const user = this.authService.getCurrentUser();
      params.customerName = user.customerorgname;
    }

    // if role is LeasingPartner or EndCustomer, only ACTIVE invoices are fetched
    if (role === UserRole.LeasingPartner || role === UserRole.EndCustomer) {
      suffix = 'active';
    }
    // optional suffix
    suffix = (suffix ? `/${suffix}` : '').toLowerCase().replace(/\s*/g, '');
    return this.api
      .get(`invoices${suffix}`, { params, observe: 'response' })
      .pipe(map(res => {
        return {
          total: res.headers.get('X-Total-Count'),
          pageItems: res.body
        };
      }));
  }

  public getByStatus(status: string, params?: any): Observable<{ total: number, pageItems: Invoice[] }> {
    return this.get(params, status);
  }

  public getByInvoiceNo(invoiceNo: string): Observable<Invoice> {
    return this.api.get(`invoices/${invoiceNo}`);
  }

  // TODO: the interface of invoiceDetails needs to be defined
  public create(linearId: string, invoiceDetails) {
    return this.api.put(`invoices/${linearId}`, invoiceDetails);
  }

}
