import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Coa } from '../../models/coa.model';
import { UserRole } from '../../models/user-role.model';
import { ApiService } from '../api.service';

import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CoaService {

  constructor(private api: ApiService, private authService: AuthenticationService) { }

  /**
   * get paginated coas
   * @param params the query params
   * @param suffix the path suffix to add
   */
  public get(params?: any, suffix?: string): Observable<{ total: number, pageItems: Coa[] }> {
    // default pagination params
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);

    // if role is EndCustomer, then customer param is mandatory
    const role = this.authService.getRole();
    if (role === UserRole.EndCustomer) {
      const user = this.authService.getCurrentUser();
      params.customerName = user.customerorgname;
    }

    // the optional suffix
    suffix = (suffix ? `/${suffix}` : '').toLowerCase().replace(/\s*/g, '');
    return this.api
      .get(`coas${suffix}`, { params, observe: 'response' })
      .pipe(map(res => {
        return {
          total: res.headers.get('X-Total-Count'),
          pageItems: res.body
        };
      }));
  }

  /**
   * get by status
   * @param status the status label
   * @param params general query params
   * just a wrapper around the get method
   */
  public getByStatus(status: string, params?: any): Observable<{ total: number, pageItems: Coa[] }> {
    return this.get(params, status);
  }

  /**
   * get by id
   * @param id the id
   */
  public getById(id: string): Observable<Coa> {
    return this.api.get(`coas/${id}`);
  }

  /**
   * sign coa PUT requet
   */
  public sign(id: string, customerprintedname: string, signatoryposition: string) {
    return this.api.put(`coas/${id}/sign`, { customerprintedname, signatoryposition });
  }

}
