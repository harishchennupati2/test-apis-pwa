import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { UserRole } from '../models/user-role.model';

class RequestOptions {
  params?: any;
  headers?: HttpHeaders;
  observe?: string;
}


/**
 * this api service should be used only when user is authenticated
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private authenticationTokenType = 'Bearer';
  private apiBaseUrl: string;

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) {
    const user = this.authService.getCurrentUser();
    const role = user ? user.role : undefined;
    const { apiBase } = environment;

    if (!user || !role) {
      this.apiBaseUrl = apiBase.default;
    } else {
      this.apiBaseUrl = role === UserRole.EndCustomer ? apiBase[role][user.nodeIndex] : apiBase[role];
    }
  }

  /**
   * Performs a request with `get` http method.
   * @param url the url
   * @param options the request options
   */
  public get(url: string, options?: RequestOptions): Observable<any> {
    return this.http
      .get(this.apiBaseUrl + url, this.requestOptions(options))
      .pipe(catchError(err => this.catchError(err)));
  }

  /**
   * Performs a request with `post` http method.
   * @param url the url
   * @param body the body
   * @param options the request options
   * @param isUpload the flag if the request is made for upload
   */
  public post(url: string, body: any, options?: RequestOptions): Observable<any> {
    return this.http
      .post(this.apiBaseUrl + url, body, this.requestOptions(options))
      .pipe(catchError(err => this.catchError(err)));
  }

  /**
   * Performs a request with `put` http method.
   * @param url the url
   * @param body the body
   * @param options the request options
   */
  public put(url: string, body?: any, options?: RequestOptions): Observable<any> {
    return this.http
      .put(this.apiBaseUrl + url, body, this.requestOptions(options))
      .pipe(catchError(err => this.catchError(err)));
  }

  /**
   * Performs a request with `delete` http method.
   * @param url the url
   * @param options the request options
   */
  public delete(url: string, options?: RequestOptions): Observable<any> {
    return this.http.delete(this.apiBaseUrl + url, this.requestOptions(options))
      .pipe(catchError(err => this.catchError(err)));
  }

  /**
   * catches the auth error
   * @param error the error response
   */
  catchError(error: Response): Observable<Response> {
    if (error.status === 401) {
      this.authService.loginRedirect();
    }
    return throwError(error);
  }

  /**
   * Request options.
   * @param options - request options
   * cleans the parms and add auth headers
   */
  private requestOptions(options?: RequestOptions): any {
    options = options || new RequestOptions();
    options.headers = options.headers || new HttpHeaders();

    if (options.params) {
      Object.keys(options.params).forEach(key => {
        if (options.params[key] === null || options.params[key] === undefined) {
          delete options.params[key];
        }
      });
    }

    const accessToken = this.authService.getAccessToken();
    if (accessToken) {
      options.headers = options.headers.set('Authorization',
        `${this.authenticationTokenType} ${accessToken}`);
    }
    return options;
  }
}
