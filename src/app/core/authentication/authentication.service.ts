import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { LocalStorageConfig } from 'src/app/configs/local-storage.config';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { AvatarConfig } from 'src/app/configs/avatar.config';

import { ToastService } from '../services/toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUser: User;

  constructor(
    private router: Router,
    private http: HttpClient,
    private toastService: ToastService
  ) { }

  /**
   * fetch access token and user details given the grant code
   * once successfully fetched, save the token to localstorage
   * @param code grant code passed by sso issuer
   */
  public fetchAccessToken(code: string): Observable<boolean> {
    const { auth: { URLS, redirectURI } } = environment;
    const body = new HttpParams()
      .set('grant_type', 'authorization_code')
      .set('code', code)
      .set('redirect_uri', redirectURI);
    return this.http.get(`${URLS.backend}${URLS.getme}` + body.toString())
      .pipe(
        tap((res: { token: string }) =>{
          localStorage.setItem(LocalStorageConfig.ACCESS_TOKEN, res.token)
        }
       ),
        map((res: any) =>{
          const matchedUser = environment.users.find(u => u.userName === res.info.userName);
          const user = Object.assign(
            {},
            res,
            matchedUser ? matchedUser : {},
            {
              avatar: matchedUser ? AvatarConfig[matchedUser.role] : null,
              username: `${res.info.name.givenName} ${res.info.name.familyName}`
            }
          );
          this.currentUser = user;
          return true;
        }),
        catchError(() => {
          this.loginRedirect();
          return of(false);
        })
      )
  }

  /**
   * get whether user is logged in based on token from local storage
   * if successful,returns true
   */
  public getMe(): Observable<boolean> {
    const accessToken = this.getAccessToken();
    if (!accessToken) {
      return of(false);
    }
    this.loginRedirect();
    return of(true);
  }

  /**
   * redirect user to the sso login page
   */
  public handleSSOLogin() {
    const { auth: { URLS, clientId, redirectURI }} = environment;
    const ssoURL = `${URLS.base}${URLS.login}?client_id=${clientId}&response_type=code&redirect_uri=${redirectURI}`;
    window.location.href = ssoURL;
  }

  /**
   * get access token from storage
   */
  public getAccessToken(): string {
    return localStorage.getItem(LocalStorageConfig.ACCESS_TOKEN);
  }

  /**
   * clears local storage of all saved data
   */
  private clearLocalStorage() {
    Object.keys(LocalStorageConfig).forEach(key => {
      localStorage.removeItem(LocalStorageConfig[key]);
    });
  }

  /**
   * handles logout
   * deletes all data from local storage, calls sso logout
   * navigate to login
   */
  public logout() {
    const { auth: { URLS, logoutRedirectURI }} = environment;
    const accessToken = this.getAccessToken();
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);
    this.http.post(`${URLS.base}${URLS.logout}`, { post_logout_redirect_uri: logoutRedirectURI }, { headers }).subscribe();
    this.loginRedirect();
  }


  /**
   * redirect to login page and delete all data from local storage
   */
  public loginRedirect() {
    this.clearLocalStorage();
    this.router.navigate(['/login']);
  }

  /**
   * gets the current logged in user
   */
  public getCurrentUser(): User {
    return this.currentUser;
  }

  /**
   * gets the role of the current logged in user
   */
  public getRole(): string {
    const user = this.getCurrentUser();
    return user ? user.role : null;
  }

}
