import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { includes } from 'lodash';

import { AuthenticationService } from './authentication.service';
import { ToastService } from '../services/toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private toastService: ToastService
  ) { }

  /**
   * can activate auth guard
   * @param route - the route for which to check permission
   * if user not signed in, access is denied and user is redirected to login page
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const hasToken = !!this.authService.getAccessToken();
    const currentUser = this.authService.getCurrentUser();

    // if currentURL is login then route depending on whether user is logged-in or not
    if (includes(state.url, '/login') && state.url !== '/login/redirect') {
      if (hasToken) {
        this.router.navigate(['/login/redirect']);
        return of(false);
      }
      return of(true);
    }

    // for any other URL if user is logged-in then continue
    if (hasToken) {
      return currentUser ? of(true) : this.authService.getMe();
    }

    this.router.navigate(['/login']);
    return of(false);
  }

  // role guard, after user is authenticated
  canActivateChild(route: ActivatedRouteSnapshot): boolean {
    const role = this.authService.getRole();
    if (!role || (route.data.roles && route.data.roles.indexOf(role) === -1)) {
      this.toastService.show({ message: 'Unauthorized access!', delay: 500 });
      this.router.navigate(['/dashboard']);
      return false;
    }
    return true;
  }
}
