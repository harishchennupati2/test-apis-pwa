import { Device } from './device.model';

export class OrderItem {
  itemNumber: string;
  itemNumberSo: string;
  itemNumberCustomer: string;
  itemStatus: string;
  productNo: string;
  customerProductNo: string;
  mfgProductNo: string;
  productDescription: string;
  productLineCode: string;
  productLine: string;
  isTangible: boolean;
  isCarepack: boolean;
  isChild: boolean;
  parentNumber: string;
  bundleID: string;
  bundleName: string;
  itemCurrency: string;
  netLinePrice: string;
  netLinePriceUSD: string;
  bundlePrice: string;
  bundlePriceUSD: string;
  orderedQty: string;
  shippedQty: string;
  deliveredQty: string;
  plannedShipDate: string | Date;
  plannedDeliveryDate: string | Date;
  actualDeliveryDate: string | Date;
  carepackDetails: {
    certificateNumber: string;
    relatedProductNumber: string;
    relatedSerialNumber: string;
    devices: Device[];
  }[];
  devices?: Device[];
  orderNo?: string;
}
