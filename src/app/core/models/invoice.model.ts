import { InvoiceItem } from './invoice-item.model';

export class Invoice {
  linearId: string;
  invoiceNo: string;
  invoiceDate: string | Date;
  totalAmount: string;
  status: string;
  invoicedItems: InvoiceItem[];
}
