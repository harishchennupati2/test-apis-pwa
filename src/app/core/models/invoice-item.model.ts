import { Device } from './device.model';

export class InvoiceItem {
  serialNo: string;
  unitPrice: string;
  quantity: string;
  totalNetPrice: string;
  taxAmount: string;
  netPriceWithTax: string;
  customerName: string;
  itemNumberCustomer: string;
  productDescription: string;
  productNumber: string;
  orderNo: string;
  customerPoNumber: string;
  devices: Device[];
}
