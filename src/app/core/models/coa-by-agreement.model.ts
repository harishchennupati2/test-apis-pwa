import { Coa } from './coa.model';

export class CoaByAgreement {
  linearId: string;
  masteragreementnumber: string;
  schedulenumber: string;
  leaserorgname: string;
  customerorgname: string;
  equipmentlocation: string;
  signed: string;
  acceptancedate: string | Date;
  printname: string;
  title: string;
  coas: Coa[];
}
