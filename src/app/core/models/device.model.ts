export class Device {
  assetTag: string;
  serialNumber: string;
  boxNo: string;
  uuid: string;
  primaryMacAddress: string;
  biosVersion: string;
  imageVersion: string;
  lanMacAddress: string;
  wlanMacAddress: string;
  secondaryMacAddress1: string;
  secondaryMacAddress2: string;
  status: string;
  installDate: string | Date;
  signed: string;
}
