export class ApiErrorResponse {
  status: number;
  statusText: string;
  error: {
    status: number;
    message: string;
  };
}
