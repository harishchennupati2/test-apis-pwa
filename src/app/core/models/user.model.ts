import { UserRole } from './user-role.model';

export class User {
  role: UserRole;
  customerorgname: string;
  signatoryposition: string;
  name: {
    familyName: string;
    givenName: string;
  };
  username: string;
  avatar: string;
  nodeIndex?: number;
}
