// the texts for the COA by agreement details
// <<CLIENT_NAME>> will be replaced by ClientConfig.NAME
// and <<CUSTOMER_ORG_NAME>> will be replaced by the logged in user's customerorgname
export const ClientConfig = {
  NAME: 'SOLENHIP',

  // tslint:disable-next-line:max-line-length
  CONSIGNEE_INSTRUCTIONS: 'Goods are transported and delivered under CMR (customer) conditions. At time of delivery the following receiving checks are mandatory: Match the number of boxes and box numbers on the delivery note with the physical shipment (quantity check). Check the physical conditions of packages (quality check).\n\nIn case of obvious damage clearly specify all details of the damage on the delivery note and retain a copy. In case of obvious or concealed damage, save all packing material and delivery documents and contact the <<CLIENT_NAME>> sales office.',

  // tslint:disable-next-line:max-line-length
  MASTER_LEASE_AGREEMENT: '<<CLIENT_NAME>> (“Lessor”) and <<CUSTOMER_ORG_NAME>> (“Lessee”) are parties to the Master Lease and Financing Agreement (the “Master Agreement”) and the Schedule under such Master Agreement (the “Schedule”) identified by the Master Agreement Number and Schedule Number, respectively, specified above.  The Master Agreement and Schedule together comprise a separate Lease, a separate Financing, or a separate Lease and a separate Financing, as the case may be, that is being accepted and commenced pursuant to this Acceptance .  All capitalised terms used in this  Certificate without definition have the meanings ascribed to them in the Master Agreement.',

  // tslint:disable-next-line:max-line-length
  LEASE_ACCEPTANCE: 'All the terms and condition related the <span class="link bold">lease</span> acceptence would be documented here for the purpose of legal.',

  // tslint:disable-next-line:max-line-length
  FINANCING_ACCEPTANCE: 'All the terms and condition related the <span class="link bold">financing</span> acceptence would be documented here for the purpose of legal',

  // tslint:disable-next-line:max-line-length
  LEASE_ACKNOWLEDGEMENTS: 'All the terms and condition related the lease acknowledgement would be documented here for the purpose of legal'
};
