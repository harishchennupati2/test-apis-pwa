'use strict';
exports.__esModule = true;
var path = require('path');
var bodyParser = require('body-parser');
var express = require('express');
var fetch = require('node-fetch');
var btoa = require('btoa')
var config = require('config')
var allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
];
var Server = (function() {
  function Server() {
    var _this = this;
    this.port = 4200;
    // Create expressjs application
    this.app = express();
    //Route our backend calls
    this.app.get('/api', function(req, res) {
      return res.json({application: 'Reibo collection'});
    });

    this.app.get('/api/v1/me',async function(req,res){
        const code = req.query.code;
        const redirectUri = req.query.redirect_uri;
        const grant_type = req.query.grant_type;
        const body = {
          grant_type:grant_type,
          code: code,
          redirect_uri: redirectUri,
        };
        const clientId = config.get('clientId')
        const clientSecret = config.get('clientSecret')
        const encodedData = btoa(`${clientId}:${clientSecret}`);
        const baseURL = config.get('baseUrl')
        const headers = {
          Authorization: `Basic ${encodedData}`,
          'Content-Type': 'application/x-www-form-urlencoded',
        };
        fetch(`${baseURL}/oauth/token`,{
            method:'POST',
            headers:headers,
            body:JSON.stringify(body),
        })
        .then(response => response.json())
        .then(response => {
            let token;
            token = response.token;
            const headers = {
                'Authorization':`Bearer ${token}`
            }
            if(token){
                fetch(
                  `${baseURL}/v2/Me`,{
                      method:'GET',
                      headers:headers
                  }
                )
                .then(response2 => response2.json())
                .then(response2 => {
                    return res.json({
                        token:token,
                        info:response2
                    })
                })
            }

        })
    });
    //Redirect all the other resquests
    this.app.get('*', function(req, res) {
      if (
        allowedExt.filter(function(ext) {
          return req.url.indexOf(ext) > 0;
        }).length > 0
      ) {
        res.sendFile(path.resolve('../dist/asset-invoice-tracking/' + req.url));
      } else {
        res.sendFile(path.resolve('../dist/asset-invoice-tracking/index.html'));
      }
    });
    this.app.use(bodyParser.json({limit: '50mb'}));
    this.app.use(bodyParser.raw({limit: '50mb'}));
    this.app.use(bodyParser.text({limit: '50mb'}));
    this.app.use(
      bodyParser.urlencoded({
        limit: '50mb',
        extended: true,
      })
    );
    this.app.listen(this.port, function() {
      return console.log('http is started ' + _this.port);
    });
    this.app.on('error', function(error) {
      console.error('ERROR', error);
    });
    process.on('uncaughtException', function(error) {
      console.log(error);
    });
  }
  Server.bootstrap = function() {
    return new Server();
  };
  return Server;
})();
//Bootstrap the server, so it is actualy started
var server = Server.bootstrap();
exports['default'] = server.app;
